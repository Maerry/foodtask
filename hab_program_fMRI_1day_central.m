%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% habprogram_fMRI_1day_central.m
%%
%% script to perform 1 run of RI-10 training, 2 conditions + rest
%% 1 day version

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% modefied by Saori Tanaka
% 29/05/2006
% modified by Elizabeth Tricomi
% August 16, 2007
% modified by Maria Eckstein
% December 8, 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TD's:
% CHECK Make stimulus presentations shorter
% CHECK Make sure data is saved
% CHECK Adjust VI schedule
% CHECK Insert instructions
% CHECK Find and fix bug that prevents from saving (lines 524-529)
% CHECK Adjust number of trials [exp.number_of_trials]
   %exp.number_of_trials: original: 6 per stimulus + 8 rest [exp.number_of_trials = 20]; should be: 50 per stimulus + 75 rest fractals [exp.number_of_trials = 175]
   %DURATIONS: original: (6 fract1 + 6 fract2 + 8 rest) * 4 sec = 80 sec; should be: (50 fract1 + 50 fract2 + 75 rest) * 4 sec = 700 sec = 11min40
% CHECK Find out what ri_10_keyp & ri_10_keycor mean (are they right?)
% CHECK Find out why exp.REWARD_mm can have values other than 1
% CHECK Take out superfluous wait times
% CHECK Tell subjects in the end that they can press space
% Randomize trial order (?)
% Take out (a few) rest fractals!!
% Make different fractals differentially rewarding (?)

global session;
global subj_nr;
global exp;

session = 1;

%% Set parameters & start cogent (load s_4_params.mat, set number_of_trials, durations, etc.)
set_parameters;

%% Initialize big data matrices
init_zeros_3d = zeros(1, exp.number_of_trials, 5); % number of sessions per data, number of trials, number of intervals within fractals
init_zeros_2d = zeros(1, exp.number_of_trials);                                         % number of sessions per data, number of trials
ri_10_keyp                  = init_zeros_3d;
ri_10_keycor                = init_zeros_3d;
ri_10_timepoint.reward_time = init_zeros_3d;
ri_10_int                   = init_zeros_3d;
ri_10_trials                = init_zeros_2d;
ri_10_keyp                  = init_zeros_3d;
ri_10_keycor                = init_zeros_3d;
ri_10_points_chips          = init_zeros_2d;                       % total chips (earnings, not actual # reward displays) over trial(block)          
ri_10_points_mm             = init_zeros_2d;                          % total m&m earnings over block
ri_10_cost                  = init_zeros_2d;                             % total cost over block
ri_10_int                   = init_zeros_3d;                      % time length of each interval(time it took to get reward, or for block to time out, in s)  
ri_10_timepoint.reward_time = init_zeros_3d;  

%% Present instructions (3 slides) & rest fractal
% pr_instructions;

%% Time point 3 (Zero timepoint for data analysis: after discharged scans)
ri_10_timepoint.zero_timepoint(session) = time;

for trial = 1:exp.number_of_trials
    
    %% Initialize variables = 0 for each fractal trial
    p_total = 0;
    p_total_mm = 0;
    p_total_chips = 0;
    p_cnt = 0; %press count
    p_cor = 0; %correct press count
    
    %% Present black screen & take times for fMRI
    ri_10_timepoint.block_start = take_fMRI_times(session, trial);
    clearkeys; % clear key presses
    
    %% Initialize trial variables
    clearkeys;
    interval = 1; % counts 250ms-intervals per fractal
    time_ms = 0; % time passed since start of fractal (in msec)
    reward_num = 0;
    p_total = 0;
    availability = 0; % will this interval be rewarded?
    resp_time = 0;

    while time_ms < exp.trial_length(session, trial) %while time < trial length

        %% Initialize stuff
        reward_time = 0;
        reward_finish = 0;

        %% Prepare and present fractal & response boxes
        disp_fractal_and_response_boxes(session, trial) % prepare
        if time_ms == 0 % if first interval in trial
            t_fractal_start = cgflip(0.7, 0.7, 0.7)*1000; % present fractal -> time stamp beginning of fractal trial
            t_press_p = t_fractal_start; % time of previous press / beginning of fractal
        else
            cgflip(0.7, 0.7, 0.7); 
        end
        t_rem = (exp.trial_length(session, trial) - time_ms); % duration remaining for fractal (msec)
        if t_rem > exp.VI_time
            unit_rem = exp.VI_time; % duration of interval (exp.VI_time) / rest of fractal
        else
            unit_rem = t_rem;
        end

        %% Determine reward for next interval (10% chance of reward; stays 1 if already at 1)
        if rand < exp.VI_reward_prob
            availability = 1;
        end

        %% IF INTERVAL IS NOT YET OVER
        while unit_rem > 0 % 1 < unit_rem < 250
            t_prepress = time;
            [k, t_press, n] = waitkeydown(unit_rem, exp.active_button(exp.cond_order(session, trial))); % wait for key press
%                     unit_rem = unit_rem - (t_press - t_prepress);

            if ~isempty(t_press) %once they press a key
                %% Update time, count stuff
                time_ms = (t_press - t_fractal_start);              % time from start of fractal till button press
                p_total = p_total - 1;                              % p_total used to be money earned, now just total presses
                resp_time = resp_time + (t_press(1) - t_press_p);   % add up response time over interval(msec)
                t_press_p = t_press(1);                             % t_press_p is time of last keypress, or if it's the first keypress in interval, then it's the interval start time
                %% Count all [p_cnt] button presses
                p_cnt = p_cnt + n;                                  % count up number of keypresses in interval
                %% IF KEY PRESS IS REWARDED
                if availability == 1 && k == exp.active_button(exp.cond_order(session, trial)) % Correct key
                    %% Count button presses that led to reward [p_cor] 
                    p_cor = p_cor + n;
                    %% Display reward
                    disp_reward(session, trial);
                    reward_num = reward_num + 1;                    %count # of rewards
                    %% Take time (reward)
                    reward_time = cgflip(0.7, 0.7, 0.7);
                    wait(exp.reward_duration);                      %leave up display for exp.reward_duration
                    time_ms = time_ms + exp.reward_duration;        %add exp.reward_duration onto the time counter (time from start of block in ms)
                    %% Add reward amount, based on which currency was earned
                    switch exp.currency(exp.cond_order(session, trial))       % 1=chip, 2=m&m
                        case {1}
                            p_total_chips = p_total_chips + 1; %exp.REWARD_chips;
                        case {2}
                            p_total_mm = p_total_mm + 1; %exp.REWARD_mm;
                    end
                    %% Save results
                    ri_10_keyp(session, trial, interval) = p_cnt;                    % key presses for interval         
                    ri_10_keycor(session, trial, interval) = p_cor;                  % correct key presses for interval
                    ri_10_timepoint.reward_time(session, trial, interval) = reward_time; % reward time stamp
                    ri_10_int(session, trial, interval) = resp_time;                 % time length of each interval (time it took to get reward, or for block to time out, in s)  
                    %% Reset variables
%                   unit_rem = 0;                                   %reset unit_rem
                    interval = interval + 1;                                      %increase interval count (problem: is increased twice!!!)
                    availability = 0;                               %reset availability
                    p_cnt = 0;
                    p_cor = 0;
                    resp_time = 0;
                %% IF KEY PRESS IS NOT REWARDED
                else           
                    %% Display no-reward cue
                    disp_noreward(session, trial);
                    %% Take time
                    unit_rem = unit_rem - (t_press - t_prepress) - 50; %update time left in second
                    time_ms = time_ms + 50;                         %add the 50 ms display time onto the time counter

                end %if rew reached statement            

            else % if there was no button press in the interval
                time_ms = time_ms + unit_rem; % add time to counter
                unit_rem = 0; %reset unit_rem
            end %(if keypress)

%             interval = interval + 1;
        end % while unit_rem > 0
%             interval = interval + 1;      % increase interval count 

    end % time_ms < exp.trial_length
    t_rem = -1;     % if block is over, no reward
    time_ms = exp.trial_length(session, trial);
    resp_time = resp_time + (time - t_press_p);
    reward_time = cgflip(0.7, 0.7, 0.7); 

    ri_10_trials(session, trial)       = reward_num;                          % number of rewards in block
    ri_10_keyp(session, trial, interval)      = p_cnt;                               % key presses for interval         
    ri_10_keycor(session, trial, interval)    = p_cor;                               % correct key presses for interval
    ri_10_points_chips(session, trial) = p_total_chips;                       % total chips (earnings, not actual # reward displays) over trial(block)          
    ri_10_points_mm(session, trial)    = p_total_mm;                          % total m&m earnings over block
    ri_10_cost(session, trial)         = p_total;                             % total cost over block
    ri_10_int(session, trial, interval)       = resp_time/1000;                      % time length of each interval(time it took to get reward, or for block to time out, in s)  
    ri_10_timepoint.reward_time(session, trial, interval) = reward_time;             % reward time stamp

end

% Time point 14 (Program fnished)
ri_10_timepoint.program_fnish(session) = cgflip(0.7, 0.7, 0.7);

%record final block duration time
switch(exp.cond_order(session, trial))
    case 1
        exp.durations_dev = [exp.durations_dev (ri_10_timepoint.program_fnish(session)- ri_10_timepoint.block_start(session,trial))];
    case 2
        exp.durations_val = [exp.durations_val (ri_10_timepoint.program_fnish(session) - ri_10_timepoint.block_start(session,trial))];
    case 5
        exp.durations_rest = [exp.durations_rest (ri_10_timepoint.program_fnish(session) - ri_10_timepoint.block_start(session,trial))];
end


wait(2000);

%% total earnings display
cgpencol(0, 0, 0);
cgtext(['TOTAL EARNINGS: ' num2str(sum(sum(ri_10_points_mm)),'%1.0f') ' M&Ms'], 0, 32); %display total earnings, round to whole number
cgtext([num2str(sum(sum(ri_10_points_chips)),'%1.0f') ' chips'], 132, 0); 
cgtext('Press SPACEBAR to continue', 0, -96);
cgflip(0.7, 0.7, 0.7);
%wait for spacebar input, clear screen
waitkeydown(inf, 71);
cgflip(0.7, 0.7, 0.7);

%% save variables
ri_10_trial_length = exp.trial_length/1000; % in seconds
ri_10_onsets_dev(session,:) = exp.onsets_dev - (ri_10_timepoint.zero_timepoint(session)/1000);
ri_10_onsets_val(session,:) = exp.onsets_val - (ri_10_timepoint.zero_timepoint(session)/1000);
ri_10_onsets_rest(session,:) = exp.onsets_rest - (ri_10_timepoint.zero_timepoint(session)/1000);
ri_10_durations_dev(session,:) = exp.durations_dev;
ri_10_durations_val(session,:) = exp.durations_val;
ri_10_durations_rest(session,:) = exp.durations_rest;
eval(['save res_files\s_' num2str(subj_nr) '_session' num2str(session) '.mat ri_10*']);
if session == 2 %run devalution program if this is the last run 
    devaluation
end
stop_cogent;
