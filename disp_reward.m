function disp_reward(interval, trial)

global exp
global data

%% Draw fractal & response boxes
disp_fractal_and_response_boxes(trial);

%% Draw chips [exp.cond_order == 1] or m&m [exp.cond_order == 2]
switch exp.currency(exp.trial_cond(trial)) %currency parameter sets currency by condition, 1=frito, 2=m&m
    case {1}
        cgdrawsprite(16, 0, -180);
        cgpencol(0, 0, 0);
        data.win_mm(interval, trial)    = 1;
    case {2}
        cgdrawsprite(17, 0, -180);
        cgpencol(0, 0, 0);
        data.win_chips(interval, trial) = 1;
end

cgflip(0.7, 0.7, 0.7);
wait(exp.reward_duration);

%% Draw fractal & yellow button boxes again
disp_fractal_and_response_boxes(trial);
cgflip(0.7, 0.7, 0.7);


end