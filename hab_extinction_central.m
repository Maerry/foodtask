%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% hab_extinction.m
%%
%% script to perform 1 run of extinction test, 4 conditions + rest
%% 
%%
% modefied by Saori Tanaka
% 29/05/2006
%modified by Elizabeth Tricomi
%July 16, 2007
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 
% % add cogent directories to path
% %===============================
% addpath h:\psy131\Cogent2000v1.25\Toolbox

global session;
global subj_nr;
session = 1;
s = session;
%%
% make cogent data structure visible for debugging..
global cogent;

% reset the state of the random number generator
%===============================
rand('state',sum(100*clock));

%same for normal dist random number generator
randn('state',sum(100*clock))
%%

cgpencol(0, 0, 0);
% makecolorsprite;
cgfont('Arial', 32);
%%
%--------------------------------------------------
% Time point 1 (Cogent start)
% time returns time in ms since START_COGENT called
% ri_10_timepoint.cogent_start = time;
%--------------------------------------------------
%%
% configure the display
%===============================
%CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
%mode: 0=window, 1=full screen
%resolution 2=800x600 (others available too)
%commenting following line for now so I can test in window mode
%config_display(1, 2,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);

%config in window mode for testing: (commenting out because i have it above
%and only want 1 cogent session)
%config_display(0, 2,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);

%config_log(['Logfiles\s_' num2str(subj_nr) '_VR20_' num2str(date_nr) '_session_' num2str(session) '_exp.dat']);
%config_results(['Logfiles\s_' num2str(subj_nr) '_VR20_' num2str(date_nr) '_session_' num2str(session) '_res.res']);

% configure keyboard
%===============================
%usage: config_keyboard( quelength, resoluion, mode)
%quelength= max # of key events recorded between calls to READKEYS
%resolution: timing resolution in ms
%mode: device mode (exclusive means no other application can access
%keyboard)
%config_keyboard(10, 5, 'nonexclusive'); 
%%
% Keymap (fMRI signalもbutton responseもUSB inputで?Akeyboard扱いだって!!!)
%===============================
MRI_signal_ID = 32;
Response_box_button1_ID = 28;
Response_box_button2_ID = 29;
Response_box_button3_ID = 30;
Response_box_button4_ID = 31;

%active_button = Response_box_button1_ID;
%left_cursor = Response_box_button3_ID;
%right_cursor = Response_box_button4_ID;
%=========================================

%use keyboard for input
% keymap (from READKEYS help):
% d=4,f=6,j=10,k=11
%use active_button(COND_ORDER(s, t)) to set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters Settings
%===============================
BLOCK = 3*3; %3 cycles, 3 blocks per cycle (1/cond + 1 rest)--3 min
RI_SCHEDULE = 0.1; 
REWARD_chips = 1;
REWARD_mm = 1;

%%
% Load parameter for each subjects
%=================================
eval(['load para_files_ext\s_' num2str(subj_nr) '_paras.mat']);
% COND_ORDER: order of conditions within each run
    % 1:OTdev 2:OTval 3:UTdev 4:UTval 5:rest
% CURRENCY: maps which currency is associated with which conditions
% FIGURE_ORDER (1xnsession-1): figure-session mappingをrandomize
% TRIAL_LENGTH_VR20 (nsessionx10): length of each block in each run--20-60s
% active_button: maps correct keypress for each condition
% vr_block_order (1x10): block orderをrandomize

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                       task START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cgpencol(0, 0, 0);
makesprites;  % <=== FIGURE_ORDER, ORDER, sessionが必要
cgfont('Arial', 32);
%%
%instructions
cgflip(0.7,0.7,0.7);
cgtext('Button Press Task:', 0, 96);
cgtext('During "RESPOND" periods, try to earn food', 0, 32);
cgtext('by pressing the key corresponding to the lit square', 0, 0);
cgtext('Get ready to start.', 0, -128);
cgflip(0.7,0.7,0.7);
% waitkeydown(inf, 32); %press 5 to start
waitkeydown(inf,MRI_signal_ID);
%waits for keypress(duration, keyin)

%--------------------------------------------------
% Time point 2 (Scan onset: fMRI signal)
ri_10_timepoint.scan_onset(s) = time; 
%--------------------------------------------------

%-----------------------------------
cgsetsprite(0);
cgflip(0.7, 0.7, 0.7);

cgdrawsprite(5, 0, 0);
cgpencol(0, 0, 0);
cgdrawsprite(10, 0, 150);
cgflip(0.7, 0.7, 0.7);
wait(10000); % <== Check the discharged time
%(wait 10 s; this seems to be for discaqs)

%------------------------------------------------------
% Time point 3 (Zero timepoint for data analysis: after discharged scans)
ri_10_timepoint.zero_timepoint(s) = time;
%------------------------------------------------------

%%        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                      RI-10 and rest session
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for t = 1:BLOCK % VR condition
    
    %initialize points at zero for each trial(block)
    p_total = 0;
    p_total_mm = 0;
    p_total_chips = 0;
    p_cnt = 0; %press count
    p_cor = 0; %correct press count
    

    % draw into the offscreen area
    
    %----------------------------------------------------
    % Time point 4-13 (Each block start)
    cgsetsprite(0);
    ri_10_timepoint.block_start(s,t) = cgflip(0.7, 0.7, 0.7); %at the beginning of each block, reset screen and record time
    %"This form of cgflip returns a timestamp indicating precisely when the
    %offscreen area appeared on the monitor." 
    %----------------------------------------------------

    clearkeys; % clear key presses
    
    switch ri_10_block_order(t)

        case 2 % VI
            %==============================================================
            %
            %                        RI-10 block
            %
            %==============================================================
            

            clearkeys;

            i = 0;
            time_ms = 0
            reward_num = 0;
            p_total = 0;
            availability = 0;
            resp_time = 0;

            
            while time_ms < TRIAL_LENGTH_RI10(t)*1000
            %while time < block length
     
                % draw stimulus
                % make sprite differ by cond
                cgdrawsprite(COND_ORDER(t), 0, 0);
                %draw button press sprites
                cgdrawsprite(10, 0, 150);
                cgpencol(0, 0, 0);
%                 cgtext('RESPOND', 0, 100);
                %fill in appropriate button
                cgpencol(1,1,0);
                switch active_button(COND_ORDER(s, t))
                    case 28
                        cgrect(-90,150,50,50)
                    case 29    
                        cgrect(-30,150,50,50)
                    case 30          
                        cgrect(30,150,50,50)
                    case 31
                        cgrect(90,150,50,50)
                end
                
                if time_ms == 0 % if first interval in block
                    t_start = cgflip(0.7, 0.7, 0.7); % marks the start of the interval 
                else
                    cgflip(0.7, 0.7, 0.7); 
                end

                t_rem = (TRIAL_LENGTH_RI10(t)*1000 - time_ms); % time remaining in the block (msec)

                if t_rem > 1000
                    unit_rem = 1000; % time in 1s unit remaining
                else
                    unit_rem = t_rem;
                end
                %unit_rem = 1s unless < 1s left in block
                               
                
                if time_ms == 0
                    t_press_p = t_start*1000; % hold time of previous press (msec)
                % = time block started in ms
                else    
                    t_press_p = time_ms + t_start*1000;
                 % = time from start of block to end of prev rew display + time block started    
                end

                reward_time = 0;
                reward_finish = 0;

                %determine reward availabity for the next second
                %10% chance of reward; stays 1 if already at 1
                if rand < RI_SCHEDULE
                    availability = 1;
                end
                
                
%                 while p_cor < ratio & t_rem > 0 % 1loop 
                    %while # correct keypresses < # needed for reward & there's
                    %still time left in the block
                while unit_rem > 0
                    
                    t_prepress = time;
                    % wait up to 1 s for keypress
%                     [k t_press n] = waitkeydown(1000, active_button);
                    [k t_press n] = waitkeydown(unit_rem, active_button(COND_ORDER(t)));
                    %active_button(COND_ORDER(s, t))-only correct key works
                    if ~isempty(t_press) %once they press a key
                        time_ms = 1000*(t_press/1000 - t_start);% time_ms + (t_press - t_press_p); % blockの始まりからのボタン押し時?? (sec)
                        
                        p_cnt = p_cnt + n; 
                         %(count up number of keypresses in interval)
                         
                         p_total = p_total - 1;               
                        % p_total used to be money earned, now just total
                        % # presses
                        
                        resp_time = resp_time + (t_press(1) - t_press_p); % add up response time over interval(msec)
                        %t_press_p is time of last keypress, or if it's the
                        %first keypress in interval, then it's the
                        %interval start time
                        t_press_p = t_press(1);                 
                        % makes t_press_p become time of previous press
                        
%                         if rand < 0.1 && unit_rem == 1000 %10% prob of reward, but only for 1st press in the second
                        if availability == 1
                            % display reward
                            cgdrawsprite(COND_ORDER(t), 0, 0);
                            % flash keypress graphic (gray circle), since it's extinction                      
                            cgdrawsprite(18, 0, -180);  
                            %draw button press sprites
%                             cgdrawsprite(10, 250, -250);
%                             cgdrawsprite(10, -250, -250);
                            cgdrawsprite(10, 0, 150);
                            cgpencol(0, 0, 0);
%                             cgtext('RESPOND', 0, 100);
                            %fill in appropriate button
                            cgpencol(1,1,0);
                    switch active_button(COND_ORDER(s, t))
                        case 28
                            %cgrect(160,-250,50,50)
                            cgrect(-90,150,50,50)
                        case 29
                            %cgrect(220,-250,50,50)
                            cgrect(-30,150,50,50)
                        case 30
                            % cgrect(280,-250,50,50)
                            cgrect(30,150,50,50)
                        case 31
                            % cgrect(340,-250,50,50)
                            cgrect(90,150,50,50)
                    end
                             % variable that only counts rewarded presses
                        if k == active_button(COND_ORDER(t))
                        p_cor = p_cor + n;
                        end

%                             % figure reward
%                             %-------------------
% 
%                             switch CURRENCY(COND_ORDER(s, t)) %currency parameter sets currency by condition, 1=colones, 2=rand
%                                 case {1}
%                                     cgdrawsprite(16, 0, -180);
%                                     cgpencol(0, 0, 0);
%                                     cgtext('Win!', 0, -100);
%                                     cgtext('+1 chip', 0, -250);
%                                 case {2}
%                                     cgdrawsprite(17, 0, -180);
%                                     cgpencol(0, 0, 0);
%                                     cgtext('Win!', 0, -100);
%                                     cgtext('+1 M&M', 0, -250);
%                             end
                            %------------------------------------------------
                            % Time point (Reward timing)
                            reward_time = cgflip(0.7, 0.7, 0.7);
                            %------------------------------------------------

                            wait(50); %leave up display for 50 ms during extinction test
                            unit_rem = 0; %reset unit_rem
                            availability = 0 %reset availability
                            time_ms = time_ms + 50;
                            %add the time onto the time counter
                            reward_num = reward_num + 1;
                            %count # of rewards
                            

%                             %add reward amount, based on which currency was earned
%                             switch CURRENCY(COND_ORDER(s, t)) % 1=chip, 2=m&m
%                                 case {1}
%                                     p_total_chips = p_total_chips + REWARD_chips;
% 
%                                 case {2}
%                                     p_total_mm = p_total_mm + REWARD_mm;
% 
%                             end
                            %                    p_total = p_total + REWARD; % あれ?Aここでは報?Vを得るボタン押しに対するペナルティはないのかな?H

                            i = i + 1;      % increase interval count    
                            
                            ri_10_keyp(s, t, i) = p_cnt;      % key presses for interval         
                            ri_10_keycor(s, t, i) = p_cor;     % correct key presses for interval
                            ri_10_timepoint.reward_time(s, t, i) = reward_time; % reward time stamp
                            ri_10_int(s, t, i) = resp_time/1000; % time length of each interval(time it took to get reward, or for block to time out, in s)  
                            
                            %reset p_cnt and p_cor
                            p_cnt = 0;
                            p_cor = 0;
                            %reset resp_time
                            resp_time = 0;
                        
                        else %(if keypress is not rewarded)   
                        % flash keypress graphic (gray circle)                      
                        cgdrawsprite(COND_ORDER(t), 0, 0);
                        %draw button press sprites
%                         cgdrawsprite(10, 250, -250);
%                         cgdrawsprite(10, -250, -250);
                        cgdrawsprite(10, 0, 150);
                        cgpencol(0, 0, 0);
%                         cgtext('RESPOND', 0, 100);
                        cgdrawsprite(18, 0, -180);
                        %fill in appropriate button
                        cgpencol(1,1,0);
                    switch active_button(COND_ORDER(s, t))
                        case 28
                            %cgrect(160,-250,50,50)
                            cgrect(-90,150,50,50)
                        case 29
                            %cgrect(220,-250,50,50)
                            cgrect(-30,150,50,50)
                        case 30
                            % cgrect(280,-250,50,50)
                            cgrect(30,150,50,50)
                        case 31
                            % cgrect(340,-250,50,50)
                            cgrect(90,150,50,50)
                    end
                        cgflip(0.7, 0.7, 0.7);

                        wait(50);
                        cgdrawsprite(COND_ORDER(t), 0, 0);
                        %draw button press sprites
%                         cgdrawsprite(10, 250, -250);
%                         cgdrawsprite(10, -250, -250);
                        cgdrawsprite(10, 0, 150);
                        cgpencol(0, 0, 0);
%                         cgtext('RESPOND', 0, 100);
                        %fill in appropriate button
                        cgpencol(1,1,0);
                    switch active_button(COND_ORDER(s, t))
                        case 28
                            %cgrect(160,-250,50,50)
                            cgrect(-90,150,50,50)
                        case 29
                            %cgrect(220,-250,50,50)
                            cgrect(-30,150,50,50)
                        case 30
                            % cgrect(280,-250,50,50)
                            cgrect(30,150,50,50)
                        case 31
                            % cgrect(340,-250,50,50)
                            cgrect(90,150,50,50)
                    end
                        cgflip(0.7, 0.7, 0.7);
                        
                        unit_rem = unit_rem - (t_press - t_prepress) - 50 %update time left in second

                        % store time of presses
%                         vr_20_times{t} = [vr_20_times{t} (t_press - t_start*1000)/1000];

%                         resp_time = resp_time + (t_press(1) - t_press_p); % add up response time over interval(msec)
                        %t_press_p is time of last keypress, or if it's the
                        %first keypress in interval, then it's the
                        %interval start time
                        
                        time_ms = time_ms + 50;
                            %add the 50 ms display time onto the counter

                        t_rem = TRIAL_LENGTH_RI10(t)*1000 - time_ms; 
                        %(time remaining in block)
%                         p_cnt = p_cnt + n;                   
                        %(count up number of keypresses in interval)
%                         % variable that only counts correct presses
%                         if k == active_button(COND_ORDER(t))
%                         p_cor = p_cor + n;
%                         end
%                         p_total = p_total - 1;               
                        % p_total used to be money earned, now just total
                        % # presses
%                         t_press_p = t_press(1);                 
                        % makes t_press_p become time of previous press
                        
                        end %if rew reached statement
                  
                     % (if interval timed out)--need to fix this with the new structure
                        
                    else
                        time_ms = time_ms + unit_rem % add time to counter
                        unit_rem = 0 %unit times out if no keypress
                    end %(if keypress)
                end % while unit_rem > 0
                        
            
                %--------------------------------------------------
                
%                 if p_cor == ratio       % reward if correct keypresses have reached the ratio 
%                 if p_cor == ratio & t_rem > 1000    % reward if keypresses have reached the ratio 
                                      
                
                % calculate earnings in dollars (i think since you can't
                % combine chips and m$ms into 1 value, we don't need this
%                 earnings = rate_rand .* p_total_rand + rate_colones .* p_total_colones + p_total/400;
                      
                
            end % while: 1 block
            %this next part used to be up higher, when button press waited
            %whole interval length; now that won't work
            t_rem = -1;     % if block is over, no reward
            i = i + 1;      % increase interval count 
            time_ms = TRIAL_LENGTH_RI10(t)*1000
            resp_time = resp_time + (time - t_press_p);
            reward_time = cgflip(0.7, 0.7, 0.7); %what is the point of this line?
            
                ri_10_trials(s, t) = reward_num;  %number of rewards in block           
                ri_10_keyp(s, t, i) = p_cnt;      % key presses for interval         
                ri_10_keycor(s, t, i) = p_cor;     % correct key presses for interval
                ri_10_points_chips(s, t) = p_total_chips;     % total points (earnings, in colones) over trial(block)          
                ri_10_points_mm(s, t) = p_total_mm;   %total rand earnings over block
                ri_10_cost(s, t) = p_total; %total cost over block
                ri_10_int(s, t, i) = resp_time/1000; % time length of each interval(time it took to get reward, or for block to time out, in s)  
                ri_10_timepoint.reward_time(s, t, i) = reward_time; % reward time stamp
                
        case 0 % rest
            %==============================================================
            %
            %                    REST block
            %
            %==============================================================
            % draw stimulus
            cgdrawsprite(COND_ORDER(t), 0, 0);
            cgpencol(0, 0, 0);
%             cgtext('REST', 0, 100);
            %draw button press sprites
%             cgdrawsprite(10, 250, -250);
%             cgdrawsprite(10, -250, -250);
            cgdrawsprite(10, 0, 150);
            cgflip(0.7, 0.7, 0.7);
            wait(TRIAL_LENGTH_RI10(t)*1000)

    end % switch between VR and rest


end % t (1-10 block)

%calculate earnings (commented because I did this at interval level and
%then summed that instead)
%earnings = rate_rand .* (sum(vr_20_points_rand(s,:))) + rate_colones .* (sum(vr_20_points_colones(s,:))) + (sum(vr_20_cost(s,:)))/100 

% display total points for trial (run)
%--------------------------------------
%earnings display for 1 run
cgpencol(0, 0, 0);
cgtext('AMOUNT EARNED: 0 M&Ms', 0, 32);
cgtext('0 chips', 132, 0);
% cgtext(['COST: ' num2str(sum(vr_20_cost(s,:))) ' cents'], 80, -32);
cgtext('Press SPACEBAR to continue', 0, -96);
%-----------------------------------------------------
% Time point 14 (Program fnished)
ri_10_timepoint.program_fnish(s) = cgflip(0.7, 0.7, 0.7);
%-----------------------------------------------------
%format output with 2 decimal places
%format bank

%wait(1000);
% putting in wait for space bar input rather than 1 s display
waitkeydown(inf, 71);

% cgtext(['$ ' num2str(sum(ri_10_earnings(s,:)), '%6.2f')], 0, 32);
% cgtext('Press SPACEBAR to continue', 0, -96);
% % show display and clear background
% cgflip(0.7, 0.7, 0.7);
% %wait for spacebar input
% waitkeydown(inf, 71);

%%
% total earnings display

% TotEarn = sum(sum(ri_10_earnings)) + sum(ri_10_winnings_c) + sum(ri_10_winnings_r); %calculate total earnings
% ri_10_totearn(s) = TotEarn; %save total earnings variable

% cgtext(['TOTAL EARNINGS TODAY: ' num2str(sum(sum(ri_10_points_mm))) ' M&Ms'], 0, 32); %display total earnings in bank format
% cgtext([num2str(sum(sum(ri_10_points_chips))) ' chips'], 132, 0); 
% cgtext('Press SPACEBAR to continue', 0, -96);
% cgflip(0.7, 0.7, 0.7);
%%
%wait for spacebar input, clear screen
% waitkeydown(inf, 71);
% cgflip(0.7, 0.7, 0.7);

%  pleasantness ratings
%--------------------------------------
%run pleasantness function, and save out ratings to vr_20_pleas
%ratings are saved in condition order
% ri_10_pleas = pleasantness;
    
% save variables
%------------------
ri_10_trial_length = TRIAL_LENGTH_RI10;
eval(['save res_files_ext\s_' num2str(subj_nr) '_RI10_results.mat ri_10*']);
% eval(['save res_files\s_' num2str(subj_nr) '_VR10_wks.mat *']);

