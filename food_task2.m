function food_task2

global exp;
global data;

%% Set parameters & start cogent (number_of_trials, durations, etc.)
set_parameters;

%% Present instructions (3 slides) & rest fractal
pr_instructions;

%% Conduct the experiment
for trial = 1:exp.numb_of_trials.food

    clearkeys;                                  % clear keys at beginning of each trial
    cgflip(0.7, 0.7, 0.7);

    %% Summary feedback
    if exp.trial_cond(trial) == 5
        
        % Tell subjects how much money they won!
        disp_summary_feedback(trial);
        
    %% Response fractal
    elseif any(exp.trial_cond(trial) == [1, 2])
                
        % Loop through intervals
        for interval = 1:ceil(exp.trial_length(trial)/exp.interval_duration)
        
            % Present fractal & yellow button box; and take time
            disp_fractal_and_response_boxes(trial);
            t_interval_start = cgflip(0.7, 0.7, 0.7) * 1000;
            
            % Wait for button press (maximally wait until interval is over)
            [key, key_time, key_n] = ...
                waitkeydown(exp.interval_duration - (time - t_interval_start));
            
            % If no button, jump over rest and start next interval
            if isempty(key)
            % If not the right button, record button press, but do not give feedback
            elseif key ~= exp.active_button(exp.trial_cond(trial))
                data.fal_key(interval, trial)  = 1;         % record false key press
                data.key_time(interval, trial) = key_time;
                
            % If the right button was pressed, give feedback (and record key presses)
            else
                if data.reward_schedule(interval, trial)    % reward scheduled for current interval
                    disp_reward(interval, trial);
                    data.reward_received(interval, trial) = 1;
                else                                        % no reward scheduled for current interval
                    disp_noreward(trial);
                end
                data.cor_key(interval, trial)  = key_n;     % record correct key presses
                data.key_time(interval, trial) = key_time;
                    
                % If there is still time left in the interval, wait for more key presses (and record them), but don't give any more rewards
                while time < t_interval_start + exp.interval_duration       % if less than 250ms have passed since fractal came on
                
                    [key, ~, key_n] = ...                                   % record button presses
                        waitkeydown(exp.interval_duration - (time - t_interval_start));
                    
                    if ~isempty(key)
                        if key == exp.active_button(exp.trial_cond(trial))
                            disp_noreward(trial);                               % show no-reward for correct buttons
                        end
                        data.cor_key(interval, trial) = data.cor_key(interval, trial) + key_n;
                    end
            
                end                 % when the interval is finally over, start the next interval
            end                 % when no key (or not the right key) has been pressed, start the next interval
        end                 % when all intervals of the current fractal have been presented, save the data & move to the next trial
    end                 % start break or fractal trial, depending on exp.trial_cond...
end                 % ... until all trials in exp.numb_of_trials.food have been shown

%% Save data
perform_file_name = [exp.results_filepath, sprintf('Results/FOOD_performance_%03s_%i%s.mat', exp.subj, exp.session, exp.version)];
params_file_name  = [exp.results_filepath, sprintf('Results/FOOD_params_%03s_%i%s.mat',      exp.subj, exp.session, exp.version)];
save(perform_file_name, 'data');
save(params_file_name, 'exp');

%% Give feedback on money bonus
pr_money_feedback

%% Overall feedback(?) & Say goodbye
stop_cogent;

end
