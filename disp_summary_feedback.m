function disp_summary_feedback(trial)

global exp
global data

%% Determine number of points won
%%% If there was no summary feedback yet
if ~any(exp.trial_cond(1:(trial-1)) == 5)
    % count rewards from the beginning of the task
    trial_with_last_feedback = 0;
%%% If there already was a summary feedback
else
    % cound the rewards since then
    trial_with_last_feedback = find(exp.trial_cond(1:(trial-1)) == 5, 1, 'last');
end
%%% Count the rewards
points_won_since_last_feedback = sum(sum(data.reward_received(:,(trial_with_last_feedback+1):trial)));

%% Write and display the message
m1 = ['You won ', num2str(points_won_since_last_feedback), ' points in the last ', num2str(exp.summary_feedback_trial.food), ' trials!'];
m2 = 'Press space to go back to the task and collect more points!';

cgpencol(0, 0, 0);
cgflip(0.7,0.7,0.7);
cgtext(m1, 0,  200); %'You won X points in the last Y trials!'
cgtext(m2, 0, -200); %'Press space to go back to the task and collect more points!';
cgflip(0.7,0.7,0.7);
waitkeydown(inf, 71);


