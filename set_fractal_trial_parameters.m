function set_fractal_trial_parameters(trial)

global exp;
global tr;
global data;

%% In what interval(s) of the fractal will rewards be available?
tr.reward_schedule = rand(1, ceil(exp.trial_length(trial)/exp.interval_duration)) < 0.1;

%% In what interval(s) of the fractal were rewards actually given?
tr.win_mm = zeros(1, length(tr.reward_schedule));
tr.win_chips = zeros(1, length(tr.reward_schedule));

%% In what interval(s) of the fractal were correct and false buttons pressed?
tr.cor_key = zeros(1, length(tr.reward_schedule));
tr.fal_key = zeros(1, length(tr.reward_schedule));


end
