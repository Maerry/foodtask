function rating_pref = preference
%Pleasantness ratings
% asks subjects to choose which fractal they prefer
% Elizabeth Tricomi
% Jan 3, 2007
%last modified Apr 26, 2007


% present all 120 pairs...
global subj_nr;
% initialize rating matrix
rating_pref = [];

% Load parameter for each subjects
%=================================
eval(['load para_files\s_' num2str(subj_nr) '_paras.mat']);

%make sprites with fractal patterns
makesprites

clearpict(7);
cgfont('Arial',30);
cgpencol(0,0,0)
offset = 0;
k = 0;
spritenum = 1;
TOTAL = 3;
CYCLE = 3;
%%
disp_order = randperm(TOTAL);
side = randperm(2);
active_button = 59;
left_cursor = 97;
right_cursor = 98;

pairs = [5 2; 5 1; 2 1];
% food_type = ['a Frito' 'an M&M'];
%instructions
cgflip(0.7, 0.7, 0.7);
wait(100)

%%
% preparestring ('Thanks for participating in today''s experiment.', 8, 0, 64);
preparestring ('For each of the following pairs of fractals,', 8, 0, 32);
preparestring('please choose which you prefer.', 8, 0, 0);
preparestring('Press SPACEBAR to continue.', 8, 0, -96);
drawpict(8);
waitkeyup(inf, 71);
clearpict(8);
cgflip(0.7,0.7,0.7);
%show each pair of fractals in random order.
%%
%3 cycles
for cycle = 1:CYCLE

%make text and scroll bar
    preparestring('Please choose which of the fractals below you prefer.', 7, 0, 200);
    preparestring('Use LEFT and RIGHT ARROW buttons to scroll left and right.', 7, 0, 160);
    preparestring('Press ENTER to select.', 7, 0, 120);

%% 
    cgdrawsprite(pairs(disp_order(spritenum),side(1)), -125, 0);
    cgdrawsprite(pairs(disp_order(spritenum),side(2)), 125, 0);

%%    
drawpict(7);
%%
 % return font to previous size
    cgfont('Arial', 30);
drawpict(7);
%%
%update screen as subject scrolls and chooses
for trial = 1:TOTAL    
    
    offset = pairs(disp_order(spritenum),side(1));
    non_pref = pairs(disp_order(spritenum),side(2));
%%
    cgpenwid(5);
    cgpencol(1,1,1);
    cgdraw(-225, -100, -25, -100);
    cgdraw(-225, 100, -25, 100);
    cgdraw(-225, -100, -225, 100);
    cgdraw(-25, -100, -25, 100);
    cgflip;
%%    
    while k ~= active_button

     % draw cursor
%     cgpencol(0,0,0) 
%     cgdraw(-5 + offset, -210, 5 + offset, -210);
 
    %drawpict(7);

    % wait for keypress and move cursor
    [k t n] = waitkeyup(inf);
%     waitkeyup(inf);
%     cgflip  
%     cgpencol(0.7,0.7,0.7)
% %     cgdraw(-5 + offset, -210, 5 + offset, -210);
%     cgdraw(-225, -100, -25, -100);
%     cgdraw(-225, 100, -25, 100);
%     cgdraw(-225, -100, -225, 100);
%     cgdraw(-25, -100, -25, 100);
    cgpencol(1,1,1);
    
    %clearpict(7);
    %cgflip(0.70, 0.70, 0.70);
%     [k t n] = getkeyup;
        if k == left_cursor
            cgdraw(-225, -100, -25, -100);
            cgdraw(-225, 100, -25, 100);
            cgdraw(-225, -100, -225, 100);
            cgdraw(-25, -100, -25, 100);
            cgpencol(0.7,0.7,0.7)
            cgdraw(25, 100, 225, 100);
            cgdraw(25, -100, 225, -100);
            cgdraw(225, -100, 225, 100);
            cgdraw(25, -100, 25, 100);
            offset = pairs(disp_order(spritenum),side(1));
            non_pref = pairs(disp_order(spritenum),side(2));
             cgflip
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             if offset == -350
%                 offset = offset;
%             else
%                 offset = offset - 70;
%             end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif k == right_cursor
            cgdraw(25, 100, 225, 100);
            cgdraw(25, -100, 225, -100);
            cgdraw(225, -100, 225, 100);
            cgdraw(25, -100, 25, 100);
            cgpencol(0.7,0.7,0.7)
            cgdraw(-225, -100, -25, -100);
            cgdraw(-225, 100, -25, 100);
            cgdraw(-225, -100, -225, 100);
            cgdraw(-25, -100, -25, 100);
            offset = pairs(disp_order(spritenum),side(2));
            non_pref = pairs(disp_order(spritenum),side(1));
             cgflip
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             if offset == 350
%                 offset = offset;
%             else
%                 offset = offset + 70;
%             end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
%         cgflip
    end
%%
%once it is the active button, record the rating

rating_pref(cycle,trial, 1) = offset; 
rating_pref(cycle,trial, 2) = non_pref;  %record non-preferred fractal # too
%reset k
k = 0;
%reset offset (resetting at trial start instead)
% offset = pairs(disp_order(spritenum),side(1));
% clearpict(7);
% cgflip; 
cgfont('Arial', 30);    
    
 
%waitkeydown(inf, 71);
%%
% update sprite number 
if spritenum < TOTAL
spritenum = spritenum + 1;
else spritenum = spritenum;
end

% draw next fractal
side = randperm(2);
 cgpencol(0.7,0.7,0.7) %gray pen color
%  cgdraw(-5 + offset, -210, 5 + offset, -210); %gray out old cursor
    cgpenwid(5);
   
    cgdraw(-225, -100, -25, -100);
    cgdraw(-225, 100, -25, 100);
    cgdraw(-225, -100, -225, 100);
    cgdraw(-25, -100, -25, 100);
       
    cgdraw(25, 100, 225, 100);
    cgdraw(25, -100, 225, -100);
    cgdraw(225, -100, 225, 100);
    cgdraw(25, -100, 25, 100);
%%
%  cgdrawsprite(disp_order(spritenum), 0, 0); %draw new fractal
cgdrawsprite(pairs(disp_order(spritenum),side(1)), -125, 0);
    cgdrawsprite(pairs(disp_order(spritenum),side(2)), 125, 0);
 
cgflip;
%%
% cgdrawsprite(disp_order(spritenum), 0, 0); %draw new fractal again so buffer has it too
%gray out old cursors again
 cgpencol(0.7,0.7,0.7) %gray pen color
%  cgdraw(-5 + offset, -210, 5 + offset, -210); %gray out old cursor
    cgpenwid(5);
   
    cgdraw(-225, -100, -25, -100);
    cgdraw(-225, 100, -25, 100);
    cgdraw(-225, -100, -225, 100);
    cgdraw(-25, -100, -25, 100);
       
    cgdraw(25, 100, 225, 100);
    cgdraw(25, -100, 225, -100);
    cgdraw(225, -100, 225, 100);
    cgdraw(25, -100, 25, 100);
    
cgdrawsprite(pairs(disp_order(spritenum),side(1)), -125, 0);
    cgdrawsprite(pairs(disp_order(spritenum),side(2)), 125, 0);
%waitkeydown(inf, 71);
%%
end %for loop   
disp_order = randperm(TOTAL); %remix order
offset = 0; %reset variables
k = 0;
spritenum = 1;
end %cycle loop
cgflip(0.7,0.7,0.7);
cgflip(0.7,0.7,0.7);