function ri_10_timepoint__block_start = take_fMRI_times(trial)

global exp

% Time point 4-13 (Each block start)
cgsetsprite(0);
ri_10_timepoint__block_start(trial) = cgflip(0.7, 0.7, 0.7); %at the beginning of each block, reset screen and record time
%"This form of cgflip returns a timestamp indicating precisely when the
%offscreen area appeared on the monitor." 

% record onset times into different vectors for each condition
switch(exp.trial_cond(trial))
    case 1
        exp.onsets_dev  = [exp.onsets_dev  ri_10_timepoint__block_start(trial)];
    case 2
        exp.onsets_val  = [exp.onsets_val  ri_10_timepoint__block_start(trial)];
    case 5
        exp.onsets_rest = [exp.onsets_rest ri_10_timepoint__block_start(trial)];
end

% record block duration times into different vectors for each condition
if trial > 1 %so that it doesn't get stuck on block 1 
    switch(exp.trial_cond(trial - 1)) 
        case 1
            exp.durations_dev  = [exp.durations_dev  (ri_10_timepoint__block_start(trial) - ri_10_timepoint__block_start(trial - 1))];
        case 2
            exp.durations_val  = [exp.durations_val  (ri_10_timepoint__block_start(trial) - ri_10_timepoint__block_start(trial - 1))];
        case 5
            exp.durations_rest = [exp.durations_rest (ri_10_timepoint__block_start(trial) - ri_10_timepoint__block_start(trial - 1))];
    end
end

end
