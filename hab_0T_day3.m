%hab_0T_day3.m
%runs extinction test and ratings for before and after fMRI sessions
%Elizabeth Tricomi
%last modified 6/26/07

clear all;      %clear all variables

%global ORDER    %allows script to access ORDER variable
%may be able to comment that out--I never use ORDER variable, since runs
%are all the same
global session; %run
global day; %training day
global subj_nr;
input('START EXPERIMENT! (enter)');

% Input subject's ID number
%=================================
subj_nr1 = input('SUBJECT NUMBER: ');
subj_nr2 = input('SUBJECT NUMBER AGAIN: ');

if subj_nr1 == subj_nr2
    subj_nr = subj_nr1;
else
    error('input correct subject number!')
end
day = input('DAY: ');
date_nr = input('DATE(ex. 20060606): ');
%%
% Load parameter (variables) for each subject  
%=================================
eval(['load para_files\s_' num2str(subj_nr) '_paras.mat']);
% ORDER (1xnsession): session order��randomize
%clear A vi_4_block_order vr_20_block_order TRIAL_LENGTH %clears these variables
%(not really sure why these need to be cleared; I'll comment out for now)

% SESSION = 4;
%(total number of *training* sessions (runs) you want to run--don't count extinction test)
%%
%initialize matrices for speed
% vr_20_trials = [];
% vr_20_ratio = [];
% vr_20_keyp = [];         
% vr_20_keycor = [];
% vr_20_points_colones = [];
% vr_20_points_rand = [];
% vr_20_cost = [];
% vr_20_earnings = [];
% vr_20_int = []; 
% vr_20_timepoint.reward_time = [];
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        EXPERIMENT START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
%mode: 0=window, 1=full screen
%config in full screen mode(res4 = 1152x864)
config_display(0, 2,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);
% 
config_log(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_exp.dat']);
config_results(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_res.res']);
% 
% % configure keyboard
% %===============================
% %usage: config_keyboard( quelength, resoluion, mode)
% %quelength= max # of key events recorded between calls to READKEYS
% %resolution: timing resolution in ms
% %mode: device mode (exclusive means no other application can access
% %keyboard)
config_keyboard(10, 5, 'nonexclusive'); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        PROGRAM START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
start_cogent;
%%

% ri_10_pleas_day3start = pleasantness;
% ri_10_pref_day3start = preference;
% 
% ri_10_rating_hunger_start = hunger;
% ri_10_rating_food_start = pleasantness_food;
% 
% eval(['save res_files_ext\s_' num2str(subj_nr) '_RI10_results.mat ri_10*']);
% stop_cogent;
%%
%need a screen that says fMRI experiment goes here
% cgpencol(0,0,0)
% cgtext('Thanks.  The fMRI scan will take place now.', 0, 96);
% % cgtext('Press SPACEBAR to begin', 0, -128);
% cgflip(0.7,0.7,0.7);
% waitkeydown(inf, 71); %press spacebar to move on
%%
%food and hunger ratings--these should be at start of extinction program
%now
% ri_10_rating_food_end = pleasantness_food;
% ri_10_rating_hunger_end = hunger;
%%
%run extinction test 

% Load parameter (variables) for each subject  
%=================================
eval(['load para_files_ext\s_' num2str(subj_nr) '_paras.mat']);
%%
%input(['START SESSION No.' num2str(session + 1) ' (press enter)'])
session = 5;
hab_extinction
%%
% run script to obtain pleasantness ratings--do this before or after
% extinction test?
ri_10_pleas_end = pleasantness;
ri_10_pref_end = preference;
eval(['save res_files_ext\s_' num2str(subj_nr) '_RI10_results.mat ri_10*']);
stop_cogent;