
global exp

figure_name = {...
    'c_pink.bmp',...
    'c_blue.bmp',...
    'c_brown.bmp',...
    'c_green.bmp',...
    'c_orange.bmp',...
    'c_purple.bmp',...
    'c_red.bmp',...
    'c_yellow.bmp',...
    };

%=========================================
% cond 1 sprite 
%-----------------------------------------
cgmakesprite(1, 160, 160, 0.7, 0.7, 0.7);  %make a sprite with dimensions 160x160, dark gray
cgsetsprite(1);

% FRACTAL 1
%randomize by condition, based on exp.figure_order
cgloadbmp(1,figure_name{exp.figure_order(1)},160,160);

% FRACTAL 2
cgmakesprite(2, 160, 160, 0.7, 0.7, 0.7);  %make a sprite with dimensions 160x160, dark gray
cgsetsprite(2);
cgloadbmp(2,figure_name{exp.figure_order(2)},160,160);
        
%% keypress sprites
cgmakesprite(10, 230, 50, 0.7, 0.7, 0.7);
cgsetsprite(10);
cgpencol(1,1,0);        %yellow color to draw squares
cgrect(-90,0,50,50);    %left one
cgrect(90,0,50,50);     %right one
cgpencol(0.7,0.7,0.7);  %grey color to remove inners of squares
cgrect(-90,0,40,40);    %left one
cgrect(90,0,40,40);     %right one

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% add reward sprites %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

cgmakesprite(16, 130, 100, 0.7, 0.7, 0.7);
cgsetsprite(16);
cgloadbmp(16,'money.bmp',0,0);

cgmakesprite(17, 100, 100, 0.7, 0.7, 0.7);
cgsetsprite(17);    
cgloadbmp(17,'money.bmp',0,0);

%making blackcircle sprite to use as keypress graphic
cgmakesprite(18, 100, 100, 0.7, 0.7, 0.7);
cgsetsprite(18);    
cgloadbmp(18,'darkgraycircle_ongray.BMP',100,100);

% return to black
cgpencol(0, 0, 0);
cgsetsprite(0)
