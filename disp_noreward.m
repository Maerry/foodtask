function disp_noreward(trial)

%% Add grey circle to fractal
% Prepare fractal & button boxes
disp_fractal_and_response_boxes(trial);
% Add black circle (sprite 18)
cgpencol(0, 0, 0);
cgdrawsprite(18, 0, -180);
% Draw whole sprite
cgflip(0.7, 0.7, 0.7);

wait(50);

%% Draw fractal & yellow button boxes again
disp_fractal_and_response_boxes(trial);
cgflip(0.7, 0.7, 0.7);


end


