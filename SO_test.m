function rating_SO = SO_test
%test of knowledge of stim-outcome associations
% asks subjects to rate food pleasantness
% Elizabeth Tricomi
% Jan 3, 2007
%last modified July 18, 2007

global subj_nr;
% initialize rating matrix
rating_SO = [];

% Load parameter for each subjects
%=================================
eval(['load para_files\s_' num2str(subj_nr) '_paras.mat']);

%make sprites with fractal patterns
makesprites;

clearpict(7);
cgfont('Arial',30);
cgpencol(0,0,0)
offset = 0;
k = 0;
spritenum = 1;
TOTAL = 2;
%%

fractals = [1 2];
disp_order = randperm(2); 
active_button = 59;
left_cursor = 97;
right_cursor = 98;

%instructions
cgflip(0.7, 0.7, 0.7);
wait(100)

%%

%make text and scroll bar
    preparestring('When the fractal below was shown, was it more likely', 7, 0, 240);
    preparestring('that a button press would result in a frito or an M&M reward?', 7, 0, 200);
    preparestring('Use LEFT and RIGHT ARROW buttons to scroll left and right.', 7, 0, 160);
    preparestring('Press ENTER to select.', 7, 0, 120);
    % label selector bar
    preparestring('M&M much', 7, -330, -120);
    preparestring('more likely', 7, -330, -150);
    preparestring('Frito much', 7, 330, -120);
    preparestring('more likely', 7, 330, -150);
    preparestring('not sure', 7, 0, -150);
    %put numbers on bar
    preparestring('0', 7, 0, -180);
    
    preparestring('1', 7, 70, -180);
    preparestring('-1', 7, -70, -180);

    preparestring('2', 7, 140, -180);
    preparestring('-2', 7, -140, -180);
   
    preparestring('3', 7, 210, -180);
    preparestring('-3', 7, -210, -180);
 
    preparestring('4', 7, 280, -180);
    preparestring('-4', 7, -280, -180);

    preparestring('5', 7, 350, -180);
    preparestring('-5', 7, -350, -180);
    %draw first fractal (randomized with disp_order variable)
%     cgdrawsprite(disp_order(spritenum), 0, 0);
    cgdrawsprite(fractals(disp_order(spritenum)), 0, 0);
    % draw lines
    cgpenwid(5);
    cgdraw(-350, -200, 350, -200);
    x1 = -70*(-5:1:5);
    y1 = -205*ones(1,11);
    x2 = -70*(-5:1:5);
    y2 = -195*ones(1,11);
    cgdraw(x1, y1, x2, y2);
    
drawpict(7);
%%
 % return font to previous size
    cgfont('Arial', 30);
drawpict(7);

%update screen as subject scrolls and chooses
for fractal = 1:TOTAL    
    
    while k ~= active_button

     % draw cursor
    cgpencol(0,0,0) 
    cgdraw(-5 + offset, -210, 5 + offset, -210);
 cgflip;
 
    %drawpict(7);

    % wait for keypress and move cursor
    waitkeyup(inf);
    cgflip  
    cgpencol(0.7,0.7,0.7)
    cgdraw(-5 + offset, -210, 5 + offset, -210);
    
    %clearpict(7);
    %cgflip(0.70, 0.70, 0.70);
    [k t n] = getkeyup;
        if k == left_cursor
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if offset == -350
                offset = offset;
            else
                offset = offset - 70;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif k == right_cursor
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if offset == 350
                offset = offset;
            else
                offset = offset + 70;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
    end

%once it is the active button, record the rating
% rating_SO(fractal) = offset/70; 
rating_SO(fractals(disp_order(spritenum))) = offset/70

%reset k
k = 0;
%reset offset
offset = 0;
%clearpict(7);
cgflip; 
cgfont('Arial', 30);    
    
%waitkeydown(inf, 71);

% update sprite number 
if spritenum < 2
spritenum = spritenum + 1;
else spritenum = spritenum;
end

% draw next fractal
 cgpencol(0.7,0.7,0.7) %gray pen color
 cgdraw(-5 + offset, -210, 5 + offset, -210); %gray out old cursor
 cgdrawsprite(disp_order(spritenum), 0, 0); %draw new fractal

cgflip;
cgdrawsprite(disp_order(spritenum), 0, 0); %draw new fractal again so buffer has it too
%waitkeydown(inf, 71);

end %for loop   
cgpencol(0,0,0);
cgflip(0.7,0.7,0.7);
cgtext('Please wait for experimenter before continuing.', 7, 0, 120);
cgflip(0.7,0.7,0.7);
waitkeydown(inf, 90);