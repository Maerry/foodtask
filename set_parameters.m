function set_parameters

global exp;
global data;

%% Parameters of the task
exp.date                        = exp.data.date1;
exp.interval_duration           =  500;
exp.reward_duration             =  100;
exp.trial_duration              = 4000;
exp.break_duration              = 5000;
exp.interval_reward_prob_fract1 = 0.15;   % 0.85^4 = 0.52 => One or more rewards in 48% of fract1 trials
exp.interval_reward_prob_fract2 = 0.35;   % 0.65^4 = 0.18 => One or more rewards in 83% of fract2 trials 

%% Get parameters (previously loaded for each subject; now freshly created)
% exp.currency: Which fractal is associated with m&m's, which with fritos?
exp.currency = [1, 1, 1, 2, 0];             %(fractals3&4 are never presented)

% exp.trial_cond: When are fractal1 [1] and fractal2 [2] presented; when is feedback given [5]?
exp.trial_cond = [];
exp.summary_feedback_trial.food = ceil(exp.numb_of_trials.food / exp.number_of_summary_feedbacks);   % Find out at which trials there will be summary feedback to achieve the desired number of summary feedbacks

while length(exp.trial_cond) < exp.numb_of_trials.food    % Keep adding rows as long as the vector is not long enough yet
    exp.trial_cond = [exp.trial_cond, [1 2]];%datasample([1 2], 2, 'replace', false)];
    if mod(length(exp.trial_cond), exp.summary_feedback_trial.food) == 0 || mod(length(exp.trial_cond) - 1, exp.summary_feedback_trial.food) == 0
        exp.trial_cond = [exp.trial_cond, 5];    % Add a summary feedback when it is time, as determined above
    end
end
exp.trial_cond = exp.trial_cond(1:exp.numb_of_trials.food);    % Cut the vector to the right length
exp.trial_cond(exp.numb_of_trials.food) = 5;                   % Make the last trial a summary feedback trial

% exp.trial_length: Duration of each trial (average of fractal1 = average of fractal2 = exp.trial_duration = 4)
exp.trial_length = zeros(1, exp.numb_of_trials.food);
count_fract1     = sum(exp.trial_cond == 1);
count_fract2     = sum(exp.trial_cond == 2);
fract1_durations = zeros(1, count_fract1);
fract2_durations = zeros(1, count_fract2);
while mean(fract1_durations) < (exp.trial_duration - 100) || ...
      mean(fract1_durations) > (exp.trial_duration + 100)
  fract1_durations = datasample([exp.trial_duration + 500, exp.trial_duration, exp.trial_duration - 500], count_fract1); %0.5 * exp.trial_duration + exp.trial_duration * rand(1, count_fract1); % for a mean of 4 sec, 2 < trial_duration < 6
end
while mean(fract2_durations) < (exp.trial_duration - 100) || ...
      mean(fract2_durations) > (exp.trial_duration + 100)
  fract2_durations = datasample([exp.trial_duration + 500, exp.trial_duration, exp.trial_duration - 500], count_fract2); %0.5 * exp.trial_duration + exp.trial_duration * rand(1, count_fract2); % for a mean of 4 sec, 2 < trial_duration < 6
end
exp.trial_length(exp.trial_cond == 1) = fract1_durations;
exp.trial_length(exp.trial_cond == 2) = fract2_durations;

% exp.figure_order: Which fractals correspond to exp.trail_cond 1 and 2
if strcmp(exp.version, 'A')
    exp.figure_order = datasample(1:2, 2, 'replace', false);
elseif strcmp(exp.version, 'B')
    exp.figure_order = datasample(3:4, 2, 'replace', false);
elseif strcmp(exp.version, 'C')
    exp.figure_order = datasample(5:6, 2, 'replace', false);
elseif strcmp(exp.version, 'D')
    exp.figure_order = datasample(7:8, 2, 'replace', false);
end

% exp.active_buttion: Which buttons go with which fractal
exp.active_button = [datasample([17, 16], 2, 'replace', false), 0,0,0];

% rows: intervals; columns: trials
max_numb_of_intervals_per_trial = ceil(max(exp.trial_length(exp.trial_cond ~= 5)) / exp.interval_duration);
% In what interval(s) of the fractal will rewards be available?
data.reward_schedule    = zeros(max_numb_of_intervals_per_trial, exp.numb_of_trials.food);
size_fract1_matrix      = [max_numb_of_intervals_per_trial, sum(exp.trial_cond == 1)];
size_fract2_matrix      = [max_numb_of_intervals_per_trial, sum(exp.trial_cond == 2)];
data.reward_schedule(:,exp.trial_cond == 1) = rand(size_fract1_matrix) < exp.interval_reward_prob_fract1;
data.reward_schedule(:,exp.trial_cond == 2) = rand(size_fract2_matrix) < exp.interval_reward_prob_fract2;
% In what interval(s) of the fractal were rewards actually given?
data.reward_received    = zeros(max_numb_of_intervals_per_trial, exp.numb_of_trials.food);
% In what interval(s) of the fractal were correct and false buttons pressed?
data.cor_key            = zeros(max_numb_of_intervals_per_trial, exp.numb_of_trials.food); % counts how often the correct key was pressed per intervale
data.fal_key            = zeros(max_numb_of_intervals_per_trial, exp.numb_of_trials.food); % 1: at least one wrong key was pressed in this interval; 0: no wrong key
data.key_time           = zeros(max_numb_of_intervals_per_trial, exp.numb_of_trials.food); % time of first key in this interval
% Key presses during break fractals
data.break_key         = zeros(50, exp.numb_of_trials.food); % 
data.break_key_time    = zeros(50, exp.numb_of_trials.food); % 


%% Configure cogent stuff
%%% Log & Results
% config_log(['Logfiles\s_' num2str(exp.subj) '_RI10_' num2str(exp.date) '_exp.dat']);
% config_results(['Logfiles\s_' num2str(exp.subj) '_RI10_' num2str(exp.date) '_res.res']);

%%% Display
% CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
% mode: 0=window, 1=full screen
% resolution 2=800x600; 5=1280x1024
config_display(1, 2, [0.70 0.70 0.70], [0 0 0], 'Arial', 28, 8);

%%% Keyboard
% usage: config_keyboard( quelength, resoluion, mode)
% quelength= max # of key events recorded between calls to READKEYS
% resolution: timing resolution in ms
% mode: device mode (exclusive means no other application can access keyboard)
config_keyboard;

%%% Start Cogent
start_cogent; 
% Put fractals into buffers/sprites
makesprites;
