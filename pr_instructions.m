function pr_instructions

global exp

if any(strcmp(exp.version, {'A', 'C'}))
    m11 = 'Let me tell you how your next task works:';
    m12 = 'Your goal is to collect as many points as you can!';
    m13 = 'You will always see one of two items.';
    m13a= 'Either this one:';
    m13b= 'Or that:';
    m14 = 'To win points, you need to press the correct key for the current item.';

    m21 = 'The computer will tell you throughout the task which is the correct key.';
    m21b= '(Each picture goes with one key. The computer will NOT trick you.)';
    m21c= ' ';
    m22 = 'The LEFT yellow square indicates the LEFT key ("q")';
    m22b= 'and the RIGHT indicates the RIGHT key ("p").';
    m22c= 'Please use your left and right INDEX FINGERS to press these keys.';

    m31 = 'You can press a key as often as you wish while a picture is';
    m31b= 'on the screen. Not every single button press will give a point,';
    m32 = 'but one picture can give multiple points.';
    m33 = 'A stack of money indicates that you WON!';
    m34 = ' ';
    m35 = 'A grey circle indicates that you did not win.';
    m36 = ' ';

    m41 = 'Do you have any questions? Please raise your hand if you do.';
    m42 = 'Press space when you are ready to start.';

    %% Slide 1
    cgflip(0.7,0.7,0.7);
    cgtext(m11, 0, 270); %'Let me tell you how your next task works:';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m11, 0, 270); %'Let me tell you how your next task works:';
    cgtext(m12, 0, 200); %'Your goal is to collect as many points as you can!';
    cgtext(m13, 0, 160); %'You will always see one of two pictures.';
    cgtext(m13a, -200, 100); %'Either this one
    cgdrawsprite(1, -200, 0);
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m11, 0, 270); %'Let me tell you how your next task works:';
    cgtext(m12, 0, 200); %'Your goal is to collect as many points as you can!';
    cgtext(m13, 0, 160); %'You will always see one of two pictures.';
    cgtext(m13a, -200, 100); %'Either this one
    cgdrawsprite(1, -200, 0);
    cgtext(m13b, 200, 100); %'Or that
    cgdrawsprite(2, 200, 0);
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m11, 0, 270); %'Let me tell you how your next task works:';
    cgtext(m12, 0, 200); %'Your goal is to collect as many points as you can!';
    cgtext(m13, 0, 160); %'You will always see one of two pictures.';
    cgtext(m13a, -200, 100); %'Either this one
    cgdrawsprite(1, -200, 0);
    cgtext(m13b, 200, 100); %'Or that
    cgdrawsprite(2, 200, 0);
    cgtext(m14, 0, -150); %'To win a point, you need to press the right key for each picture.';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    %% Slide 2
    cgflip(0.7,0.7,0.7);
    cgtext(m21, 0, 270); %'The computer will tell you throughout the task which is the right key.';
    cgtext(m21b, 0, 230); %'(Each picture goes with one key and the computer will NEVER trick you.)';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m21, 0, 270); %'The computer will tell you throughout the task which is the right key.';
    cgtext(m21b, 0, 230); %'(Each picture goes with one key and the computer will NEVER trick you.)';
    cgtext(m21c, 0, 170); %' ';
    cgdrawsprite(10, 0, 100);              
    cgpencol(1, 1, 0);
    cgrect(-90, 100, 50, 50)
    cgpencol(0,0,0);
    cgtext(m22, 0, 0); %'The LEFT square indicates the LEFT key ("q")';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m21, 0, 270); %'The computer will tell you throughout the task which is the right key.';
    cgtext(m21b, 0, 230); %'(Each picture goes with one key and the computer will NEVER trick you.)';
    cgtext(m21c, 0, 170); %' ';
    cgdrawsprite(10, 0, 100);              
    cgpencol(1, 1, 0);
    cgrect(90, 100, 50, 50)
    cgpencol(0,0,0);
    cgtext(m22, 0, 0); %'The LEFT square indicates the LEFT key ("q")';
    cgtext(m22b, 0,-60); %'and the RIGHT indicates the RIGHT key ("p").';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m21, 0, 270); %'The computer will tell you throughout the task which is the right key.';
    cgtext(m21b, 0, 230); %'(Each picture goes with one key and the computer will NEVER trick you.)';
    cgtext(m21c, 0, 170); %' ';
    cgdrawsprite(10, 0, 100);              
    cgpencol(1, 1, 0);
    cgrect(90, 100, 50, 50)
    cgpencol(0,0,0);
    cgtext(m22, 0, 0); %'The LEFT square indicates the LEFT key ("q")';
    cgtext(m22b, 0,-60); %'and the RIGHT indicates the RIGHT key ("p").';
    cgtext(m22c, 0,-150); %'Please use your left and right INDEX FINGERS to press these keys.';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    %% Slide 3
    cgflip(0.7,0.7,0.7);
    cgtext(m31, 0, 260); %'You can press a key as often as you wish';
    cgtext(m31b, 0, 220); %'while a picture is on the screen.';
    cgtext(m32, 0, 180); %'(Not every button press will give a point.)';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m31, 0, 260); %'You can press a key as often as you wish';
    cgtext(m31b, 0, 220); %'while a picture is on the screen.';
    cgtext(m32, 0, 180); %'(Not every button press will give a point.)';
    cgtext(m33, 0, 130); %'A stack of money indicates that you WON!';
    cgtext(m34, 0, 150); %' ';
    cgdrawsprite(16, 0, 60);
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m31, 0, 260); %'You can press a key as often as you wish';
    cgtext(m31b, 0, 220); %'while a picture is on the screen.';
    cgtext(m32, 0, 180); %'(Not every button press will give a point.)';
    cgtext(m33, 0, 130); %'A stack of money indicates that you WON!';
    cgtext(m34, 0, 150); %' ';
    cgdrawsprite(16, 0, 60);
    cgtext(m35, 0,-50); %'When you did not win,';
    cgtext(m36, 0,-80); %'you will see a grey circle:';
    cgdrawsprite(18, 0,-180);
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    %% Last Slide
    cgflip(0.7,0.7,0.7);
    cgtext(m41, 0,  100); %Do you have any questions? Please raise your hand if you do.';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    cgtext(m41, 0,  100);
    cgtext(m42, 0, -100); %'Press space when you are ready to start.';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);
    
else
    m11 = 'Welcome back!';
    m12 = 'Try to collect as many points as you can by pressing the correct';
    m13 = 'key for each picture, just like before!';
    m14 = 'Press q for the left squre and p for the right.';
    
    m41 = 'Do you have any questions? Please raise your hand if you do.';
    m42 = 'Press space when you are ready to start.';
    
    %% Slide 1
    cgflip(0.7,0.7,0.7);
    cgtext(m11, 0, 270); %'Welcome back!';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    %% Slide 2
    cgflip(0.7,0.7,0.7);
    cgtext(m11, 0, 270); %'Welcome back!';
    cgtext(m12, 0, 230); %'Try to collect as many points as you can by pressing the right';
    cgtext(m13, 0, 210); %'key for each picture, just like before!';
    cgtext(m14, 0, 190); %'Press q for the left squre and p for the right.';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

    %% Slide 2
    cgflip(0.7,0.7,0.7);
    cgtext(m11, 0, 270); %'Welcome back!';
    cgtext(m12, 0, 230); %'Try to collect as many points as you can by pressing the right';
    cgtext(m13, 0, 210); %'key for each picture, just like before!';
    cgtext(m14, 0, 190); %'Press q for the left squre and p for the right.';
    cgtext(m41, 0,  100); %'Do you have any questions? Please raise your hand if you do.';
    cgtext(m42, 0, -100); %'Press space when you are ready to start.';
    cgflip(0.7,0.7,0.7);
    waitkeydown(inf, 71);

end

