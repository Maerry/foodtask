function pr_money_feedback

global exp
global data

%% Calculate task bonus
received_points = sum(sum(data.reward_received));
min_points      = exp.numb_of_trials.food * (exp.interval_reward_prob_fract1 + ...
    exp.interval_reward_prob_fract2) / 2;   % Number of rewards if button is only pressed once per trial
max_points      = sum(sum(data.reward_schedule));   % Number of rewards if right button is pressed in every interval

if received_points < min_points
    received_points = min_points;
end

bonus = (received_points - min_points) / (max_points - min_points) * ...
    exp.bonus.max_session/6;

%% Store task bonus (to calculate total bonus at the end)
if any(strcmp(exp.version, {'A', 'C'}))
    exp.bonus.task1_run1 = bonus;
elseif any(strcmp(exp.version, {'B', 'D'}))
    exp.bonus.task1_run2 = bonus;
end

%% Prepare and present message
m11 = 'Great job!';
m12 = ['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
m13 = '(This bonus is based on your performance and added';
m14 = 'to your regular payment.)';
m15 = 'Press space when you want to move on to your next task.';

cgflip(0.7,0.7,0.7);
cgtext(m11, 0, 270); %'Great job!';
cgtext(m12, 0, 200); %['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
cgtext(m13, 0, 150); %'(This bonus is based on your performance and added';
cgtext(m14, 0, 100); %'to your regular payment.)';
cgflip(0.7,0.7,0.7);
waitkeydown(inf, 71);
cgtext(m11, 0, 270); %'Great job!';
cgtext(m12, 0, 200); %['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
cgtext(m13, 0, 150); %'(This bonus is based on your performance and added';
cgtext(m14, 0, 100); %'to your regular payment.)';
cgtext(m15, 0,-250); %'Press space when are ready for your next task.';
cgflip(0.7,0.7,0.7);
waitkeydown(inf, 71);
