function rating_hunger = hunger
%Pleasantness ratings
% asks subjects to rate food pleasantness
% Elizabeth Tricomi
% Jan 3, 2007
%last modified Apr 24, 2007

global subj_nr;
% initialize rating matrix
rating_hunger = [];

% Load parameter for each subjects
%=================================
eval(['load para_files\s_' num2str(subj_nr) '_paras.mat']);

%make sprites with fractal patterns
makesprites

clearpict(7);
cgfont('Arial',30);
cgpencol(0,0,0)
offset = 0;
k = 0;
spritenum = 1;
TOTAL = 1;
%%
disp_order = [16 17];
active_button = 59;
left_cursor = 97;
right_cursor = 98;
% food_type = ['a Frito' 'an M&M'];
%instructions
%cgflip(0.7, 0.7, 0.7);
wait(100)
%%
% preparestring ('Thanks for participating in today''s experiment.', 8, 0, 64);
% preparestring ('Please rate the pleasantness of each', 8, 0, 32);
% preparestring('of the following fractal patterns.', 8, 0, 0);
% preparestring('Press SPACEBAR to continue.', 8, 0, -96);
% drawpict(8);
% waitkeyup(inf, 71);
% clearpict(8);
% cgflip(0.7,0.7,0.7);

%show sprites 1-5 in random order.
%%

%make text and scroll bar
    preparestring('Please rate your current hunger level.', 7, 0, 200);
    preparestring('Use LEFT and RIGHT ARROW buttons to scroll left and right.', 7, 0, 160);
    preparestring('Press ENTER to select.', 7, 0, 120);
    % label selector bar
    preparestring('very', 7, -340, -120);
    preparestring('full', 7, -340, -150);
    preparestring('very', 7, 340, -120);
    preparestring('hungry', 7, 340, -150);
    %put numbers on bar
    preparestring('5', 7, 0, -180);
    
    preparestring('6', 7, 70, -180);
    preparestring('4', 7, -70, -180);

    preparestring('7', 7, 140, -180);
    preparestring('3', 7, -140, -180);
   
    preparestring('8', 7, 210, -180);
    preparestring('2', 7, -210, -180);
 
    preparestring('9', 7, 280, -180);
    preparestring('1', 7, -280, -180);1

    preparestring('10', 7, 350, -180);
    preparestring('0', 7, -350, -180);
    %draw first fractal (randomized with disp_order variable)
%     cgdrawsprite(disp_order(spritenum), 0, 0);
    % draw lines
    cgpenwid(5);
    cgdraw(-350, -200, 350, -200);
    x1 = -70*(-5:1:5);
    y1 = -205*ones(1,11);
    x2 = -70*(-5:1:5);
    y2 = -195*ones(1,11);
    cgdraw(x1, y1, x2, y2);
    
drawpict(7);
%%
 % return font to previous size
    cgfont('Arial', 30);
drawpict(7);

%update screen as subject scrolls and chooses
for food = 1:TOTAL    
    
    while k ~= active_button

     % draw cursor
    cgpencol(0,0,0) 
    cgdraw(-5 + offset, -210, 5 + offset, -210);
 cgflip;
 
    %drawpict(7);

    % wait for keypress and move cursor
    waitkeyup(inf);
    cgflip  
    cgpencol(0.7,0.7,0.7)
    cgdraw(-5 + offset, -210, 5 + offset, -210);
    
    %clearpict(7);
    %cgflip(0.70, 0.70, 0.70);
    [k t n] = getkeyup;
        if k == left_cursor
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if offset == -350
                offset = offset;
            else
                offset = offset - 70;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif k == right_cursor
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if offset == 350
                offset = offset;
            else
                offset = offset + 70;
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
    end

%once it is the active button, record the rating
rating_hunger(food) = offset/70 + 5; 
 
%reset k
k = 0;
%reset offset
offset = 0;
%clearpict(7);
cgflip; 
cgfont('Arial', 30);    
    
 
%waitkeydown(inf, 71);

% update sprite number 
if spritenum < 2
spritenum = spritenum + 1;
else spritenum = spritenum;
end

% draw next fractal
 cgpencol(0.7,0.7,0.7) %gray pen color
 cgdraw(-5 + offset, -210, 5 + offset, -210); %gray out old cursor
%  cgdrawsprite(disp_order(spritenum), 0, 0); %draw new fractal

 
cgflip;
% cgdrawsprite(disp_order(spritenum), 0, 0); %draw new fractal again so buffer has it too
%waitkeydown(inf, 71);

end %for loop   
cgflip(0.7,0.7,0.7);
