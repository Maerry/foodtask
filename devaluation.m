% devalution
% determines which reward will be devalued and shows screen indicating
% all-you-can eat bonus for that reward
% Elizabeth Tricomi
% last modified 4/19/07


%choose devalued reward (going to choose devalued rew based on currency
%variable instead)
% deval = randperm(2);

%make bulk reward sprites 
cgmakesprite(19, 100, 100, 0.7, 0.7, 0.7);
cgsetsprite(19);
cgloadbmp(19,'fritos-bulk.BMP',100,100);

cgmakesprite(20, 100, 100, 0.7, 0.7, 0.7);
cgsetsprite(20);
cgloadbmp(20,'m&m--bulk.BMP',100,100);

cgsetsprite(0); %reset sprite
cgflip(0.7,0.7,0.7);
% hmm, should pic be where fractal goes or where reward goes...?

% switch deval(1)
%(going to choose devalued rew based on currency variable instead)
switch CURRENCY(1) %find currency associated with Cond 1, 1=chips, 2=m&ms
    case 1 %chips devalued
        %bonus screen
        cgdrawsprite(19, 0, -180);
        cgpencol(0, 0, 0);        
        cgtext(['TOTAL EARNINGS TODAY: ' num2str(sum(sum(ri_10_points_mm)),'%1.0f') ' M&Ms'], 0, 102); %display total earnings in bank format
        cgtext([num2str(sum(sum(ri_10_points_chips)), '%1.0f') ' chips'], 150, 70); 
        cgtext('BONUS!', 0, 0);
        cgtext('All you can eat chips!', 0, -32);
        cgflip(0.7,0.7,0.7);
        waitkeydown(inf, 71); %wait for Padenter key to continue
    case 2 % m&ms devalued
        %bonus screen
        cgdrawsprite(20, 0, -180);
        cgpencol(0, 0, 0);
        cgtext(['TOTAL EARNINGS TODAY: ' num2str(sum(sum(ri_10_points_mm)),'%1.0f') ' M&Ms'], 0, 102); %display total earnings in bank format
        cgtext([num2str(sum(sum(ri_10_points_chips)), '%1.0f') ' chips'], 150, 70); 
        cgtext('BONUS!', 0, 0);
        cgtext('All you can eat M&M''S!', 0, -32);
        cgflip(0.7,0.7,0.7);
        waitkeydown(inf, 71); %wait for Padenter key to continue
end