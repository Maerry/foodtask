%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% hab_0T_1day.m 
%%
%% script to run habit learning training paradigm, 6 6-min runs
%%
% modefied by Saori Tanaka
% 15/06/2006
% modified by Elizabeth Tricomi
% July 16, 2007
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
clear all;      %clear all variables

%global ORDER    %allows script to access ORDER variable
%may be able to comment that out--I never use ORDER variable, since runs
%are all the same

global session; %run
global day; %training day
global subj_nr;
input('START EXPERIMENT! (enter)');

% Input subject's ID number
%=================================
subj_nr1 = input('SUBJECT NUMBER: ');
subj_nr2 = input('SUBJECT NUMBER AGAIN: ');

if subj_nr1 == subj_nr2
    subj_nr = subj_nr1;
else
    error('input correct subject number!')
end

day = input('DAY: ');
date_nr = input('DATE(ex. 20060606): ');
%%
% Load parameter (variables) for each subject  
%=================================
eval(['load para_files\s_' num2str(subj_nr) '_paras.mat']);
% ORDER (1xnsession): session order��randomize
%clear A vi_4_block_order vr_20_block_order TRIAL_LENGTH %clears these variables
%(not really sure why these need to be cleared; I'll comment out for now)

SESSION = 2;
%(set to total number of sessions (runs) you want to run)
%%
%initialize matrices for speed
ri_10_cost = [];
ri_10_int = [];
ri_10_keyp = [];         
ri_10_keycor = [];
ri_10_pleas_orig = [];
ri_10_points_chips = [];
ri_10_points_mm = [];
ri_10_trials = [];

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        EXPERIMENT START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
%mode: 0=window, 1=full screen
%config in full screen mode: (res 5: 1280x1024; 2(800x600); 4(1152x864))
config_display(0, 2,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);

config_log(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_exp.dat']);
config_results(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_res.res']);

% configure keyboard
%===============================
%usage: config_keyboard( quelength, resoluion, mode)
%quelength= max # of key events recorded between calls to READKEYS
%resolution: timing resolution in ms
%mode: device mode (exclusive means no other application can access
%keyboard)
config_keyboard(10, 5, 'nonexclusive'); 
%--------------------------------------------------
% Time point 1 (Cogent start)
% time returns time in ms since START_COGENT called
ri_10_timepoint.cogent_start = time;
%--------------------------------------------------
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        PROGRAM START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
start_cogent;

%hunger and food pleasantness ratings
ri_10_rating_hunger_start = hunger;
ri_10_rating_food_start = pleasantness_food;

%run fractal pleasantness/preference ratings on day 1 only

ri_10_pleas_orig = pleasantness;
ri_10_pref = preference;

stop_cogent;
eval(['save res_files\s_' num2str(subj_nr) '_RI10_results_day' num2str(day) '.mat ri_10*']);

hab_practice_central;


% for session = (SESSION*day - 1):SESSION*day
%    
%    input(['START SESSION No.' num2str(session) ' (press enter)'])
%     
%     hab_program_0T_1day_central
%     
% end

%need a screen for while they eat...

%CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
%mode: 0=window, 1=full screen
%config in full screen mode: (res 5: 1280x1024; 2(800x600); 4(1152x864))
config_display(1, 2,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);

config_log(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_exp.dat']);
config_results(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_res.res']);

% configure keyboard
%===============================
config_keyboard(10, 5, 'nonexclusive'); 
start_cogent;
cgflip(0.7,0.7,0.7);
% cgtext(['TOTAL EARNINGS TODAY: ' num2str(sum(sum(ri_10_points_mm))) ' M&Ms'], 0, 32); %display total earnings in bank format
% cgtext([num2str(sum(sum(ri_10_points_chips))) ' chips'], 132, 0); 
cgtext('Thanks, the fMRI scan will take place now.', 0, 0);
cgflip(0.7, 0.7, 0.7);
% 
waitkeydown(inf, 90); %leave up screen until PadEnter is hit

%put in prob of fractal going with each food here
ri_10_rating_SO = SO_test;

ri_10_rating_food_end = pleasantness_food;
ri_10_rating_hunger_end = hunger;
eval(['save res_files\s_' num2str(subj_nr) '_RI10_results_day' num2str(day) '.mat ri_10*']);
%%
% extinction test on last day
% hab_extinction_central

ri_10_pleas_end = pleasantness;
ri_10_pref_end = preference;
eval(['save res_files_ext\s_' num2str(subj_nr) '_RI10_results.mat ri_10*']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        PROGRAM STOP!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop_cogent;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        EXPERIMENT FINISHED!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Debug_Script_VIVR
