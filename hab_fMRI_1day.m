%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% hab_fMRI_1day.m 
%%
%% script to run habit learning training paradigm, 2 8ish-min runs, +
%% extinction test
%%
% modefied by Saori Tanaka
% 15/06/2006
% modified by Elizabeth Tricomi
% July 16, 2007
% modified by Maria Eckstein
% Dez 08, 2015
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
clear all;      %clear all variables

global session; %run
global day; %training day
global subj_nr;
input('START EXPERIMENT! (enter)');

% Input subject's ID number
%=================================
subj_nr1 = input('SUBJECT NUMBER: ');
subj_nr2 = input('SUBJECT NUMBER AGAIN: ');

if subj_nr1 == subj_nr2
    subj_nr = subj_nr1;
else
    error('input correct subject number!')
end

day = input('DAY: ');
date_nr = input('DATE(ex. 20060606): ');

%%
% Load parameter (variables) for each subject  
%=================================
% eval(['load para_files\s_' num2str(subj_nr) '_paras.mat']);


SESSION = 2;
%(set to total number of sessions you want to run)
%%
%initialize matrices for speed
ri_10_cost = [];
ri_10_int = [];
ri_10_keyp = [];         
ri_10_keycor = [];
ri_10_pleas_orig = [];
ri_10_points_chips = [];
ri_10_points_mm = [];
ri_10_trials = [];
ri_10_onsets_dev = [];
ri_10_onsets_val = [];
ri_10_onsets_rest = [];
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        EXPERIMENT START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
%mode: 0=window, 1=full screen
% %config in full screen mode: (res 5: 1280x1024; 2(800x600); 4(1152x864))
% config_display(0, 5,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);
% 
% config_log(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_exp.dat']);
% config_results(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_res.res']);

% configure keyboard
%===============================
%usage: config_keyboard( quelength, resoluion, mode)
%quelength= max # of key events recorded between calls to READKEYS
%resolution: timing resolution in ms
%mode: device mode (exclusive means no other application can access
%keyboard)
% config_keyboard(10, 5, 'nonexclusive'); 
%--------------------------------------------------
% Time point 1 (Cogent start)
% time returns time in ms since START_COGENT called
ri_10_timepoint.cogent_start = time;
%--------------------------------------------------
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        PROGRAM START!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for session = (SESSION*day - 1):SESSION*day
   
   input(['START SESSION No.' num2str(session) ' (press enter)'])
    
    hab_program_fMRI_1day_central
    
end

clear ri*
%CONFIG_DISPLAY( mode, resolution, background, foreground, fontname, fontsize, nbuffers, number_of_bits, scale )
%mode: 0=window, 1=full screen
%config in full screen mode: (res 5: 1280x1024; 2(800x600); 4(1152x864))
config_display(1, 5,[0.70 0.70 0.70], [0 0 0], 'Arial', 32, 8);

config_log(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_exp.dat']);
config_results(['Logfiles\s_' num2str(subj_nr) '_RI10_' num2str(date_nr) '_session_' num2str(session) '_res.res']);

% configure keyboard
%===============================
config_keyboard(10, 5, 'nonexclusive'); 
start_cogent;
%--------------------------------------------------
% Time point 1 (Cogent start)
% time returns time in ms since START_COGENT called
ri_10_timepoint.cogent_start = time;
%--------------------------------------------------
cgflip(0.7,0.7,0.7);

% extinction test on last day

hab_extinction_central

% ri_10_pleas_end = pleasantness;
% ri_10_pref_end = preference;
eval(['save res_files_ext\s_' num2str(subj_nr) '_RI10_results.mat ri_10*']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        PROGRAM STOP!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stop_cogent;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                        EXPERIMENT FINISHED!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

