function disp_fractal_and_response_boxes(trial)

global exp

%% Draw fractal
cgdrawsprite(exp.trial_cond(trial), 0, 0); % fractal

%% Draw response boxes
cgdrawsprite(10, 0, 150); % empty yellow response boxes
cgpencol(1, 1, 0); % fill in the right box
switch exp.active_button(exp.trial_cond(trial))
    case 17
        cgrect(-90, 150, 50, 50)
    case 16
        cgrect(90, 150, 50, 50)
    case 0
end

end
