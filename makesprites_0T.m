%%
% make sprites for different conditions, for keypress, and for reward
% display
%===============================================
% Revision by Saori Tanaka (16/05/2006)
% Revision by Elizabeth Tricomi (May 22, 2007)


% Figure_mapping
%=================================
% referred to with FIGURE_ORDER parameter--randomized by subject
        
figure_name = {...
    'FRACTAL5-bright.bmp',...
    'FRACTAL7-bright.bmp',...
    'FRACTAL4-bright.bmp',...
    'fractal47-bright.bmp',...
    'fractal57-bright-3.bmp',...
    'fractal2-bright.bmp',...
    'fractal18-bright.bmp',...
    };
%%
        %=========================================
        % cond 1 sprite 
        %-----------------------------------------
        cgmakesprite(1, 160, 160, 0.7, 0.7, 0.7);  %make a sprite with dimensions 160x160, dark gray
        cgsetsprite(1);

        % FRACTAL 1
                
        %randomize by condition, based on FIGURE_ORDER
        cgloadbmp(1,figure_name{FIGURE_ORDER(1)},160,160);

         % FRACTAL 2
        cgmakesprite(2, 160, 160, 0.7, 0.7, 0.7);  %make a sprite with dimensions 160x160, dark gray
        cgsetsprite(2);
        cgloadbmp(2,figure_name{FIGURE_ORDER(2)},160,160);
        
        % FRACTAL 3
        cgmakesprite(3, 160, 160, 0.7, 0.7, 0.7);
        cgsetsprite(3);
        cgloadbmp(3,figure_name{FIGURE_ORDER(3)},160,160);
          
        % FRACTAL 4
        cgmakesprite(4, 160, 160, 0.7, 0.7, 0.7);  %make a sprite with dimensions 160x160, dark gray
        cgsetsprite(4);
        cgloadbmp(4,figure_name{FIGURE_ORDER(4)},160,160);
%end
%%
%keypress sprites
cgmakesprite(10, 230, 50, 0.7, 0.7, 0.7);
cgsetsprite(10);
%%
%cgpencol(1,1,0); yellow, good for fMRI, too light for goggles
cgpencol(1,0.5,0); %orange, looks yellow in goggles
cgrect(-30,0,50,50);
cgrect(-90,0,50,50);
cgrect(30,0,50,50);
cgrect(90,0,50,50);
cgpencol(0.7,0.7,0.7);
cgrect(-30,0,40,40);
cgrect(-90,0,40,40);
cgrect(30,0,40,40);
cgrect(90,0,40,40);
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% add reward sprites %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

%=========================================
% CONDITION 1 (chips)
%=========================================
% cond 1 sprite - chips
%-----------------------------------------


cgmakesprite(16, 130, 100, 0.7, 0.7, 0.7);
cgsetsprite(16);
% cgloadbmp(16,'frito_ongray2.BMP',130,100);
cgloadbmp(16,'frito_ongray.BMP',130,100);
    %set transparency color to white
%    cgtrncol(16,'w')

%=========================================
% CONDITION 2 (m&ms)
%=========================================
% cond 2 sprite - m&ms
%-----------------------------------------
cgmakesprite(17, 100, 100, 0.7, 0.7, 0.7);
cgsetsprite(17);    
    cgloadbmp(17,'redm&m_ongray.BMP',100,100);
%    cgtrncol(17,'w')


%making blackcircle sprite to use as keypress graphic
cgmakesprite(18, 100, 100, 0.7, 0.7, 0.7);
cgsetsprite(18);    
    cgloadbmp(18,'darkgraycircle_ongray.BMP',100,100);
    %maybe it's easier to just make the background gray in excel.
    %commenting out transparency color...
%    cgtrncol(18,'w')

%%
%=========================================
% REST condition
%=========================================
cgmakesprite(5, 160, 160, 0.7, 0.7, 0.7);
cgsetsprite(5);

cgloadbmp(5, figure_name{FIGURE_ORDER(5)}, 160,160);

% NOVEL REST condition
cgmakesprite(6, 160, 160, 0.7, 0.7, 0.7);
cgsetsprite(6);

cgloadbmp(6, figure_name{FIGURE_ORDER(6)}, 160,160);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% return to black
cgpencol(0, 0, 0);
cgsetsprite(0)

